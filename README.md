**Author:** Zane Globus-O'Harra

**Contact:** zfg@uoregon.edu

### Project Description

A simple webserver made with python using flask.

### Notes:

I was unable to figure out how to make it so that if `//` was in the url, it would be flagged as a 403 error. For some reason flask ignores multiple slashes in a row automatically.