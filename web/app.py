import flask
from flask import Flask, request

app = Flask(__name__)

def debug_print_url_path():
    url = request.url
    print("DEBUG: PRINT URL:", url)

    path = request.full_path
    print("DEBUG: PRINT PATH:", path)
    return path


def get_url_and_path():
    url = request.url
    path = request.full_path

    return url, path


def find_forbidden(path):
    http_response = 200

    if path.find("/..") >= 0:
        http_response = 403

    elif path.find("/~") >= 0:
        http_response = 403

    elif path.find("//") >= 0:
        http_response = 403

    return http_response


@app.route("/<string:file>")
def index(file):
    # debug_print_url_path()

    url, path = get_url_and_path()
    http_response = find_forbidden(path)

    if http_response == 403:
        return flask.render_template("403.html"), 403

    return flask.render_template(file), 200


@app.errorhandler(404)
def error_404(error):
    # debug_print_url_path()

    return flask.render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
